# ReAmplify Model

The ReAmplify (Relative Abstract Multi-Purpose-Limited Flexibility) model was developed by Stephan Ferenz[^1] and Paul Hendrik Tiemann[^2] based on discussions with Jonathan Brandt[^3] and Astrid Nieße[^1].

It is compatible with the comparison of flexibility modeles by Brandt et al. which can be found on [Gitlab](https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario).

More information wil follow soon.
 

 _____

[^1]: [Department for Computer Science, Carl von Ossietzky University of Oldenburg](https://uol.de/des)
[^2]: [OFFIS, Department Energy](https://www.offis.de/anwendungen/energie.html)
[^3]: [Leibniz University Hannover, Institute of Electric Power Systems](https://www.ifes.uni-hannover.de/de/ees/)]
